var {ipcRenderer, remote} = require('electron');

ipcRenderer.send("request", "");

function updateWidth(){
	ipcRenderer.send("resize", {
		width: $(".data-wrapper").width(),
		height: $(".data-wrapper").height()
	});
}

ipcRenderer.on('ev', (event, arg) => {
	console.log(arg);
	$("#data").html('');
	for (var i = 0; i < arg.length; i++) {
		$("#data").append(`<li><a class="launcher-item" data-launchpath="${arg[i].path}" data-sc="${arg[i].shortcut}" href="#"><img src="data:image/png;base64, ${arg[i].image}"></a></li>`);
	}

	$(".launcher-item").mousedown(function(e){
		e.preventDefault();
		if(e.which==1) ipcRenderer.send("run", $(this).data("launchpath"));
		else if(e.which==2) ipcRenderer.send("remove", $(this).data("sc"));
		else if(e.which==3) console.log('Right Mouse Button Clicked');
	});

	updateWidth();
});

(function () {
    var holder = document.getElementsByTagName('body')[0];

    holder.ondragover = () => {
        return false;
    };

    holder.ondragleave = () => {
        return false;
    };

    holder.ondragend = () => {
        return false;
    };

    holder.ondrop = (e) => {
        e.preventDefault();

        for (let f of e.dataTransfer.files) {
			ipcRenderer.send("add", f.path);
        }
        
        return false;
    };

    function init() { 
     	$(".close").click(function(){
     		ipcRenderer.send("close-w", '');
     	});
     	$(".pin").click(function(){
     		ipcRenderer.send("pin-w", '');
     	});
        $(".settings").click(function(){
            ipcRenderer.send("open-settings", '');
        });
    }

    document.onreadystatechange = function () {
        if (document.readyState == "complete") init();
    }
})();
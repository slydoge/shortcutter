const {app, BrowserWindow, ipcMain} = require('electron');

let mainWindow;

function createWindow () {
  mainWindow = new BrowserWindow({
    width: 800, 
    height: 600, 
    alwaysOnTop: true, 
    fullscreen: false,
    disableBlinkFeatures: "Auxclick",
    fullscreenable: false,
    webPreferences: {
      nodeIntegration: true
    },
    frame: false
  });

  mainWindow.loadFile('index.html');
  mainWindow.on('closed', function () {
    mainWindow = null;
  });

  mainWindow.setMenu(null);

  mainWindow.webContents.on('new-window', event => {
    event.preventDefault();
  });

  mainWindow.on('will-resize', (e) => {
    //prevent resizing even if resizable property is true.
    e.preventDefault();
  });
}
app.on('ready', createWindow);

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit();
});

app.on('activate', function () {
  if (mainWindow === null) createWindow();
});

const fs = require('fs');
var path = require('path');
var iconExtractor = require('icon-extractor');
var ws = require('windows-shortcuts');
const { exec } = require('child_process');
const chokidar = require('chokidar');

var filename = path.join(__dirname, '/shortcuts/');

var fileList = [];
var itemCounter = 0;
var totalFiles;

var targets = [];
var iconImages = [];

var inProcess = false;

var windowReady = false;

var cache = false;

function initIcons(){
  fileList = [];
  itemCounter = 0;
  totalFiles = undefined;

  targets = [];
  iconImages = [];
  cache = false;

  inProcess = false;

  fs.readdir(filename, (err, files) => {
    inProcess = true;
    files.forEach((file, i, array) => {
      fileList.push(file);
      if (i === array.length - 1) totalFiles = i;
    });

    queryItem(fileList[itemCounter]);
  });
}

initIcons();

function queryItem(item){
  ws.query(path.join(__dirname, '/shortcuts/', item), function (error, options) {
    targets.push(options.target);

    itemCounter++;
    if (itemCounter<=totalFiles) queryItem(fileList[itemCounter]);
    else{
      itemCounter = 0;
      emitSync(targets[itemCounter]);
      //iconExtractor.getIcon('SomeContextLikeAName1', targets[0]);
    }
  });
}

function emitSync(item){
  iconExtractor.getIcon('SomeContextLikeAName1', item);
}

iconExtractor.emitter.on('icon', function(data){
  iconImages.push({
    path: data.Path,
    image: data.Base64ImageData,
    shortcut: fileList[itemCounter]
  });
  
  itemCounter++;

  if (itemCounter<targets.length) emitSync(targets[itemCounter]);
  else{
    inProcess = false;
    cache = iconImages;
    if (windowReady) mainWindow.webContents.send('ev', iconImages);
    else{
      mainWindow.webContents.once('dom-ready', () => {
        mainWindow.webContents.send('ev', iconImages);
      });
    }
  }
});

setTimeout(function(){
  mainWindow.webContents.once('dom-ready', () => {
    windowReady = true;
  });
}, 100);

ipcMain.on('run', (event, arg) => { 
  console.log("Running", `"${arg}"`);
  exec(`"${arg}"`, (err, stdout, stderr) => {
    if (err) {
      // node couldn't execute the command
      console.log(err);
      return;
    }

    // the *entire* stdout and stderr (buffered)
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
  });
});

ipcMain.on('remove', (event, arg) => { 
  fs.unlinkSync(path.join(__dirname, '/shortcuts/', arg));
});

const watcher = chokidar.watch(path.join(__dirname, '/shortcuts/'), {
  ignored: /(^|[\/\\])\../,
  ignoreInitial: true,
  persistent: true
});

watcher
  .on('add', path => {
    console.log("Added");
    if(!inProcess) initIcons();
  })
  .on('change', path => {
    console.log("Changed");
    if(!inProcess) initIcons();
  })
  .on('unlink', path => {
    console.log("Removed");
    if(!inProcess) initIcons();
  });

ipcMain.on('add', (event, arg) => { 
  var parts = arg.split('\\');
  var answer = parts[parts.length - 1];
  console.log(path.join(__dirname, '/shortcuts/', answer.replace(" ", "_")+".lnk"));
  ws.create(path.join(__dirname, '/shortcuts/', answer+".lnk"), arg);
});

ipcMain.on('request', (event, arg) => { 
  if (cache) {
    mainWindow.webContents.send('ev', cache);
  }
});

ipcMain.on('resize', (event, arg) => { 
  mainWindow.setSize(arg.width, arg.height);
  console.log(arg.width, arg.height);
});

ipcMain.on('close-w', (event, arg) => { 
  mainWindow.close();
});

var pinned = true;

ipcMain.on('pin-w', (event, arg) => { 
  if (pinned) pinned = false;
  else pinned = true;
  mainWindow.setAlwaysOnTop(pinned, "floating");
});

ipcMain.on('open-settings', (event, arg) => { 
  let win = new BrowserWindow({ frame: true, width: 400, height: 200 });
});
  